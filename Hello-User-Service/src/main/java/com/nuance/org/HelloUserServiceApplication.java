package com.nuance.org;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.reactive.Response;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.nuance.org.config.HelloServiceConfiguration;


@ComponentScan("com.nuance.org")
@SpringBootApplication
@RestController
@RibbonClient(name = "hello-service", configuration = HelloServiceConfiguration.class)
public class HelloUserServiceApplication{
	
	
	
	@LoadBalanced
	 @Bean
	 RestTemplate restTemplate() {
		
	  return new RestTemplate();
	 }

	 @Autowired
	 RestTemplate restTemplate;
	 @RequestMapping(value="/hi",method = RequestMethod.GET)
	 public String hi(@RequestParam(value = "name", defaultValue = "Rafael") String name) {
		
		// System.out.println("Hello from service");
	  String greeting = this.restTemplate.getForObject("http://hello-service/greeting", String.class);
	  return String.format("%s, %s!", greeting, name);
	 }
	 
	 @RequestMapping(value="/hello",method = RequestMethod.GET)
	 public String hi() {
		
		// System.out.println("Hello from service");
	  String greeting = this.restTemplate.getForObject("http://hello-service/backend", String.class);
	  System.out.println(greeting);
	  return (greeting);
	 }
	 
	public static void main(String[] args) {
		SpringApplication.run(HelloUserServiceApplication.class, args);
	}




	
}
