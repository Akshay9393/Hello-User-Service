package com.nuance.org;

import java.io.IOException;
import java.util.logging.LogRecord;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.stereotype.Component;

@Component
public class SimpleFilter implements Filter {
   @Override
   public void destroy() {}

   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterchain) 
      throws IOException, ServletException {
      System.out.println("From registration filter");
      System.out.println("Remote getRemoteHost:"+request.getRemoteHost());
      System.out.println("Remote getRemoteAddr:"+request.getRemoteAddr());
      System.out.println("Remote getLocalAddr:"+request.getLocalAddr());
      System.out.println("Remote getLocalPort:"+request.getLocalPort());
      System.out.println("Remote getProtocol:"+request.getProtocol());
      System.out.println("Remote getRemotePort:"+request.getRemotePort());
      System.out.println("Remote getServerName:"+request.getServerName());
      System.out.println("Remote getScheme:"+request.getScheme());
      System.out.println("Remote getRemoteHost:"+request.getRemoteHost());
      System.out.println("Remote getServletContext:"+request.getServletContext());
      System.out.println("Remote getLocale:"+request.getLocale());
     // System.out.println("Remote getContentType:"+response.getContentType());
      System.out.println("Remote getOutputStream:"+response.getLocale() );
      System.out.println("Remote getOutputStream:"+response.getOutputStream() );
     // System.out.println(filterchain.doFilter(request, response));
      filterchain.doFilter(request, response);
      
   }

   @Override
   public void init(FilterConfig filterconfig) throws ServletException {
	   
	   
	   
   }
}

